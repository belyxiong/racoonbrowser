package com.bytech.racoonbrowser.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.jpush.android.api.JPushInterface;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class JPushReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context arg0, Intent intent) {
		Bundle bundle = intent.getExtras();
		// TODO Auto-generated method stub
		if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
//            System.out.println("收到了自定义消息。消息内容是：" + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            // 自定义消息不会展示在通知栏，完全要开发者写代码去处理
			Log.d("BELY","got push data:"+bundle.getString(JPushInterface.EXTRA_TITLE)+","+bundle.getString(JPushInterface.EXTRA_MESSAGE));
			
			String title = bundle.getString(JPushInterface.EXTRA_TITLE);
			String proxyserver = bundle.getString(JPushInterface.EXTRA_MESSAGE);
			if(proxyserver!=null && proxyserver.startsWith("http")){//its proxy server
				
				UrlUtils.saveProxyServer(arg0.getApplicationContext(), proxyserver);
				
				
			}
			
			
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            System.out.println("收到了通知");
            // 在这里可以做些统计，或者做些其他工作
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            System.out.println("用户点击打开了通知");

        
   
        } 
	}

}
