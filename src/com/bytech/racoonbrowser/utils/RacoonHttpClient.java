

package com.bytech.racoonbrowser.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.util.Log;

public class RacoonHttpClient {
private static final String TAG = "RacoonHttpClient";

public static JSONObject SendHttpPost(String URL, JSONObject jsonObjSend) {

	try {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httpPostRequest = new HttpPost(URL);

		StringEntity se;
		se = new StringEntity(jsonObjSend.toString());

		// Set HTTP parameters
		httpPostRequest.setEntity(se);
		//httpPostRequest.setHeader("Accept", "application/json");
		httpPostRequest.setHeader("Content-type", "text/html");
		//httpPostRequest.setHeader("Accept-Encoding", "utf-8"); // only set this parameter if you would like to use gzip compression

		long t = System.currentTimeMillis();
		HttpResponse response = (HttpResponse) httpclient.execute(httpPostRequest);
		Log.i(TAG, "HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]");

		// Get hold of the response entity (-> the data):
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			// Read the content stream
			InputStream instream = entity.getContent();
			Header contentEncoding = response.getFirstHeader("Content-Encoding");
			if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
				instream = new GZIPInputStream(instream);
			}

			// convert content stream to a String
			String resultString= convertStreamToString(instream);
			instream.close();
			resultString = resultString.substring(1,resultString.length()-1); // remove wrapping "[" and "]"

			// Transform the String into a JSONObject
			JSONObject jsonObjRecv = new JSONObject(resultString);
			// Raw DEBUG output of our received JSON object:
			Log.i(TAG,"<JSONObject>\n"+jsonObjRecv.toString()+"\n</JSONObject>");

			return jsonObjRecv;
		} 

	}
	catch (Exception e)
	{
		// More about HTTP exception handling in another tutorial.
		// For now we just print the stack trace.
		e.printStackTrace();
	}
	return null;
}


private static String convertStreamToString(InputStream is) {
	/*
	 * To convert the InputStream to String we use the BufferedReader.readLine()
	 * method. We iterate until the BufferedReader return null which means
	 * there's no more data to read. Each line will appended to a StringBuilder
	 * and returned as String.
	 * 
	 * (c) public domain: http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
	 */
	BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	StringBuilder sb = new StringBuilder();

	String line = null;
	try {
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n");
		}
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	return sb.toString();
}




/**
 * 向指定 URL 发送POST方法的请求
 * 
 * @param url
 *            发送请求的 URL
 * @param param
 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
 * @return 所代表远程资源的响应结果
 */
public static String sendPost(String url, String param) {
    PrintWriter out = null;
    BufferedReader in = null;
    String result = "";
    try {
        URL realUrl = new URL(url);
        // 打开和URL之间的连接
        URLConnection conn = realUrl.openConnection();
        // 设置通用的请求属性
        conn.setRequestProperty("accept", "*/*");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 获取URLConnection对象对应的输出流
        out = new PrintWriter(conn.getOutputStream());
        // 发送请求参数
        out.print(param);
        // flush输出流的缓冲
        out.flush();
        // 定义BufferedReader输入流来读取URL的响应
        in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
    } catch (Exception e) {
        System.out.println("发送 POST 请求出现异常！"+e);
        e.printStackTrace();
    }
    //使用finally块来关闭输出流、输入流
    finally{
        try{
            if(out!=null){
                out.close();
            }
            if(in!=null){
                in.close();
            }
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }
    return result;
}    
}
