/*
 * Zirco Browser for Android
 * 
 * Copyright (C) 2010 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.bytech.racoonbrowser.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.bytech.racoonbrowser.controllers.Controller;
import com.bytech.racoonbrowser.ui.components.UrlEncoder;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Url management utils.
 */
public class UrlUtils {

	/**
	 * Check if a string is an url.
	 * For now, just consider that if a string contains a dot, it is an url.
	 * @param url The url to check.
	 * @return True if the string is an url.
	 */
	public static boolean isUrl(String url) {
		return url.equals(Constants.URL_ABOUT_BLANK) ||
			url.equals(Constants.URL_ABOUT_START) ||
			url.contains(".");
	}
	
	/**
	 * Get the current search url.
	 * @param context The current context.
	 * @param searchTerms The terms to search for.
	 * @return The search url.
	 */
	public static String getSearchUrl(Context context, String searchTerms) {
		String currentSearchUrl = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREFERENCES_GENERAL_SEARCH_URL, Constants.URL_SEARCH_GOOGLE);
		return String.format(currentSearchUrl, searchTerms);
	}
	
	/**
	 * Check en url. Add http:// before if missing.
	 * @param url The url to check.
	 * @return The modified url if necessary.
	 */
	public static String checkUrl(String url) {
		if ((url != null) &&
    			(url.length() > 0)) {
    	
    		if ((!url.startsWith("http://")) &&
    				(!url.startsWith("https://")) &&
    				(!url.startsWith("file://")) &&
    				(!url.startsWith(Constants.URL_ABOUT_BLANK)) &&
    				(!url.startsWith(Constants.URL_ABOUT_START))) {
    			
    			url = "http://" + url;
    			
    		}
		}
		
		return url;
	}
	
	/**
	 * Check if there is an item in the mobile view url list that match a given url.
	 * @param context The current context.
	 * @param url The url to check.
	 * @return True if an item in the list match the given url.
	 */
	public static boolean checkInMobileViewUrlList(Context context, String url) {
		
		if (url != null) {
			boolean inList = false;
			Iterator<String> iter = Controller.getInstance().getMobileViewUrlList(context).iterator();			
			while ((iter.hasNext()) &&
					(!inList)) {
				if (url.contains(iter.next())) {
					inList = true;
				}
			}
			return inList;
		} else {
			return false;
		}
	}
	
	private static String[] demosticUrl = {
		"baidu.com",
		"qq.com",
		"163.com",
		"sohu.com",
		"sina.com",
		"taobao.com",
		"jd.com",
		"cctv.com",
	};
	
	public static boolean isUrlDemostic(String url){
		if(url != null && !url.equals("")){
			for(String s : demosticUrl){
				if(url.indexOf(s)>=0)
					return true;
			}
		}
		return false;
	}
	
	
	private static final String[] mDemosticServer = {
		"http://proxy.rcbaby.cn"
	};
	
	private static final String DEFAULT_PROXYSERVER = "http://api.ico.la";
	//where to find send the post
	private static final String mReqURL_DemosticServer = "/ReqProxyAddr.php";
	private static final String mReqURL_ProxyServer = "/knproxy/index.php?url=";
	
	private final static String OP_TYPE = "optype";
	private final static String OP_TYPE_QUERY = "query";
	
	private final static String OP_PARAM = "param";

	private final static String OP_PARAM_PROXYADDR = "proxyaddr";
	
	private final static String SERVER_RET_JSON_TAG_PROXYADDR = "proxyaddr";
	
	//获得可用于请求代理网址的服务器地址
	private static String getUsableDemosticReqUrl(){
		for(String url : mDemosticServer){
			String server = url + mReqURL_DemosticServer;
			if(urlReachable(server))
				return server;
		}
		return null;
	}
	
	private static boolean urlReachable(String urlStr){
		boolean available = false;

	    try {
	      URL url = new URL(urlStr);
	      HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
	      //urlConn.set
	      //urlConn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322)"); 
	      urlConn.setRequestProperty("Content-Type", "text/html; charset=gb2312");  
	      urlConn.connect();

	      int retCode = urlConn.getResponseCode();
	      String s = urlConn.getResponseMessage();
	      //urlConn.get
	      
	      available = retCode >= 200 && retCode < 400;
	    } catch (IOException e) {
	      e.printStackTrace();
	    }

	    return true;//available;
	}
	
	public static String getProxyAddr(){
		String demosticServer = getUsableDemosticReqUrl();
		if(demosticServer == null)
			return null;
		try{
			JSONObject jsonObjSend = new JSONObject();
			jsonObjSend.put(OP_TYPE, OP_TYPE_QUERY);
			jsonObjSend.put(OP_PARAM, OP_PARAM_PROXYADDR);
			
			//JSONObject jsonObjRecv = RacoonHttpClient.SendHttpPost(demosticServer, jsonObjSend);
			String ret = RacoonHttpClient.sendPost(demosticServer, OP_TYPE+"="+OP_TYPE_QUERY+"&"+OP_PARAM+"="+OP_PARAM_PROXYADDR);
			JSONObject jsonObjRecv = new JSONObject(ret);
			if(jsonObjRecv!=null && jsonObjRecv.has(SERVER_RET_JSON_TAG_PROXYADDR)){
				String proxyaddr = jsonObjRecv.getString(SERVER_RET_JSON_TAG_PROXYADDR);
				proxyaddr = proxyaddr + mReqURL_ProxyServer;
				if(urlReachable(proxyaddr))
					return proxyaddr;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String mProxy_URL = "";
	
	public static String initProxyServer(Context ctx){
		return getProxyServer(ctx);
	}
	
	final static String STR_PROXYPREF = "proxy_server";
	final static String STR_PROXYADDR = "proxy_addr";
	public static void saveProxyServer(Context ctx, String addr){
		SharedPreferences proxyPref = ctx.getSharedPreferences(STR_PROXYPREF, 0);
		proxyPref.edit().putString(STR_PROXYADDR, addr).commit();
		if("".equals(addr))
			addr = DEFAULT_PROXYSERVER;
		mProxy_URL = addr+mReqURL_ProxyServer;
	}
	
	public static String getProxyServer(Context ctx){
		SharedPreferences proxyPref = ctx.getSharedPreferences(STR_PROXYPREF, 0);
		String addr = proxyPref.getString(STR_PROXYADDR, "");
		if("".equals(addr))
			addr = DEFAULT_PROXYSERVER;
		mProxy_URL = addr+mReqURL_ProxyServer;
		return mProxy_URL;
	}
	
	static UrlEncoder mUrlEncoder;
	
	public static String decodeUrl(String encodedurl){
		String securl = encodedurl.substring(UrlUtils.mProxy_URL.length());
		if(mUrlEncoder == null)
			mUrlEncoder = new UrlEncoder();
		String decoedurl = mUrlEncoder.decode(securl);
		return decoedurl;
	}
	
	public static String encodeUrl(String originalUrl){
		String url = originalUrl;
		Log.d("BELY","encodeUrl : url 1 = "+url);
		if(!originalUrl.startsWith(UrlUtils.mProxy_URL)){
			//generate security url and use proxy
			if(mUrlEncoder == null)
				mUrlEncoder = new UrlEncoder();
			//url = "1E1h1zyv1r1e1C1tyJ190U1i12ye0H0Uy2131y1F1j1J";//ue.encode(url);
			url = mUrlEncoder.encode(originalUrl);
			url = UrlUtils.mProxy_URL+url;
		}
		Log.d("BELY","encodeUrl : url 2 = "+url);
		return url;
	}
}
