/*
 * Zirco Browser for Android
 * 
 * Copyright (C) 2010 - 2011 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.bytech.racoonbrowser.ui.components;

import android.util.Log;

public class UrlEncoder {
	

	int key = 0;
	int myLoop = 0;
	String val2k = "z0y1x2w3v4u5t6s7r8q9pAoBnCmDlEkFjGiHhIgJfKeLdMcNbOaPQRSTUVWXYZ";
	String serverKey = "joWVexlW4fHeH/2GGNLefy8bV7JFaaTTF92AWp1k0jDsMqC8tqeAvdLo/gg";
	char[] k2val = new char[125];
	public UrlEncoder(){
		myLoop = val2k.length();
		reverse_me();
	}
	
	public String convertStr(String str){ 
        //将String 对象转换为可改变的StringBuffer类对象 
        //然后调用StringBuffer类的reverse()方法实现反转 
        String strReverse=new StringBuffer(str).reverse().toString(); 
        return strReverse;     
    }
	
	public String shift_up(String s){
		int lenKey = serverKey.length();
		StringBuffer sb = new StringBuffer(s);
		StringBuffer sbKey = new StringBuffer(serverKey);
		for(int i=0;i<s.length();i++){
			//chr((ord($text[$i]) + ord($this->serverKey[$i % $lenSkey]))%256);
			char c = (char)(((int)(sb.charAt(i))+(int)(sbKey.charAt(i%lenKey)))%256);
			sb.setCharAt(i, c);
		}
		return sb.toString();
	}
	
	public String encode(String url){
		url = convertStr(url);
		//Log.d("BELY", "url after strrev = "+url);
		StringBuffer ss = new StringBuffer();
		if(serverKey.length()>0)
			url = shift_up(url);
		//Log.d("BELY", "url after shift_up = ");
		//for(int i=0;i<url.length();i++)
			//Log.d("BELY",String.valueOf((int)url.charAt(i)));
		String s = url;
		//Log.d("BELY","key="+key);
		//Log.d("BELY","myLoop="+myLoop);
		for(int i=0;i<s.length();i++){
			int ch1 = (((int)s.charAt(i))+key)%myLoop;
			int ch2 = (((int)s.charAt(i))+key-ch1)/myLoop;
			ss.append(val2k.charAt(ch2));
			ss.append(val2k.charAt(ch1));
			//Log.d("BELY","i="+i+",ch1="+ch1+",ch2="+ch2+",ss="+ss.toString());
		}
		return ss.toString();
	}
	
	private void reverse_me(){
		for(int i=0;i<val2k.length();i++){
			int value = (int)val2k.charAt(i);
			k2val[value] = (char)i;
		}
	}
	/*
function shift_down($text){
		$lenSkey = strlen($this->serverKey);
		for($i=0;$i<strlen($text);$i++){
			$text[$i] = chr((ord($text[$i]) - ord($this->serverKey[$i % $lenSkey]) + 256)%256);
		}
		return $text;
	}
	 */
	private String shift_down(String ss){
		int len = serverKey.length();
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<ss.length();i++){
			sb.append((char)((((int)ss.charAt(i))-((int)(serverKey.charAt(i % len)))+256)%256));
		}
		return sb.toString();
	}
	
	public String decode(String securl){
		StringBuffer rolling = new StringBuffer();
		String str = securl;
		
		for(int i=0;i<str.length()/2;i++){
			rolling.append((char)(k2val[str.charAt(i*2)]*myLoop + k2val[str.charAt(i*2+1)]-key));
		}
		String roll = shift_down(rolling.toString());
		return convertStr(roll.toString());
	}
	
}
