/*
 * Zirco Browser for Android
 * 
 * Copyright (C) 2010 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.bytech.racoonbrowser.ui.activities;

import com.bytech.racoonbrowser.R;
import com.bytech.racoonbrowser.utils.Constants;
import com.bytech.racoonbrowser.utils.UrlUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * About dialog activity.
 */
public class ProxyWarnActivity extends Activity {
	
	 public static Preference mProxyPreference = null;
	
	 boolean mAccepted = false;
	 @Override
	 protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 
		 Window w = getWindow();
		 w.requestFeature(Window.FEATURE_LEFT_ICON);
		 
		 setContentView(R.layout.proxywarning_activity);
		 
		 w.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,	android.R.drawable.ic_dialog_info);
		 		 

			
		
		 Button acceptBtn = (Button) this.findViewById(R.id.ProxyWarnActivity_AcceptBtn);
		 acceptBtn.setOnClickListener(new View.OnClickListener() {

			 public void onClick(View view) {  

				Toast.makeText(ProxyWarnActivity.this, getResources().getText(R.string.PreferencesActivity_EnableProxySettingsWithAD), Toast.LENGTH_LONG).show();
				UrlUtils.initProxyServer(getApplicationContext());
				Log.d("BELY", "proxyUrl = "+UrlUtils.mProxy_URL);
				mAccepted = true;
				finish();
			 }

		 });
		 
		 Button disagreeBtn = (Button) this.findViewById(R.id.ProxyWarnActivity_DisagreeBtn);
		 disagreeBtn.setOnClickListener(new View.OnClickListener() {

			 public void onClick(View view) {      
				 	if(mProxyPreference != null)
				 		((CheckBoxPreference)mProxyPreference).setChecked(false);
				 	finish();
			 }

		 });
		 

		 mAccepted = false;
	 }
	 
	 @Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
			
			switch (keyCode) {			
			case KeyEvent.KEYCODE_BACK:			
				if(mProxyPreference != null){
			 		((CheckBoxPreference)mProxyPreference).setChecked(false);
			 		
				}
				finish();
		 		return true;
			
			default: 
				return super.onKeyUp(keyCode, event);
			}
		
	 }
	 
	 @Override
	 public void onPause(){
		 super.onPause();
		 if(!mAccepted && mProxyPreference != null){
			 ((CheckBoxPreference)mProxyPreference).setChecked(false);
		 }
	 }
}
